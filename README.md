# networkboot

Ansible to setup Network Booting on FreeNAS jail

## Setup

1. Create a jail called `networkboot.rmb938.me` with an IP of `192.168.23.42`
1. Edit the jail and under basic properties check `Berkeley Packet Filter` and under jail properties check `allow_raw_sockets`
1. Open a shell and run the following:
    1. `pkg install bash python`
    1. `adduser`
        * Username: `jail-user`
        * Full name: `jail-user`
        * Uid: `1002`
        * Other Groups: `wheel`
        * Random Password
    1. `sysrc sshd_enable="YES"`
    1. `service sshd start`
1. Run `ssh-copy-id jail-user@networkboot.rmb938.me` locally
1. SSH in `ssh jail-user@networkboot.rmb938.me`

## Usage

1. Modify `hosts` and add your host
1. Build https://github.com/rmb938/k8s-on-linuxkit and copy `kube-node-initrd.img` and `kube-node-kernel` into `roles/dnsmasq/files/images`
1. Run `make`
